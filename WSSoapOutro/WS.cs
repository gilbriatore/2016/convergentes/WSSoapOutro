﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Services;

namespace WSSoapOutro
{
    [WebService (Namespace="http://localhost:9191/ws")]
    class WS : WebService
    {
        [WebMethod]
        public double somar(double a, double b) {
            return a + b;
        }
    }
}
